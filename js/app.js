//Variables
const carrito = document.getElementById('carrito');
const cursos = document.getElementById('lista-cursos');
const listaCursos = document.querySelector('#lista-carrito tbody');
const vaciarCarritoBtn = document.getElementById('vaciar-carrito');

//Event Listeners
cargarEventListeners();

function cargarEventListeners(){
	//Dispara cuando se presiona "Agregar Carrito"
	cursos.addEventListener('click',comprarCurso);

	//Cuando se elimina un curso del carrito
	carrito.addEventListener('click',eliminarCurso);

	//Vaciar carrito
	vaciarCarritoBtn.addEventListener('click',vaciarCarrito)

	//Contenido Cargado
	document.addEventListener('DOMContentLoaded',leerStorageListo);	
}


//Funciones*******************************************


//Agrega el curso al carrito
function comprarCurso(e){
	e.preventDefault();
	if(e.target.classList.contains('agregar-carrito')){
		const curso = e.target.parentElement.parentElement;
		leerDatosCurso(curso);
	}
}

//Lee los datos del curso
function leerDatosCurso(curso){
	const infoCurso = {
		imagen: curso.querySelector('img').src,
		titulo: curso.querySelector('h4').textContent,
		precio: curso.querySelector('.precio span').textContent,
		id: curso.querySelector('a').getAttribute('data-id')
	}

	insertarCarrito(infoCurso);
}

function insertarCarrito(curso){
	const row = document.createElement('tr');
	row.innerHTML = `
		<td><img src="${curso.imagen}" width=100></td>
		<td>${curso.titulo}</td>
		<td>${curso.precio}</td>
		<td><a href="#" class="borrar-curso" data-id="${curso.id}">X</a></td>
	`;

	listaCursos.appendChild(row);
	guardarCursoLocalStorage(curso);
}

//Eliminar el curso del carrito en el DOM 
function eliminarCurso(e){
	e.preventDefault();
	let curso,cursoID;
	if(e.target.classList.contains('borrar-curso')){
		e.target.parentElement.parentElement.remove();
		curso = e.target.parentElement.parentElement;
		cursoID = curso.querySelector('a').getAttribute('data-id');
	}
	eliminarCursoLocalStorage(cursoID);
}

//Elimina los cursos del carrito en el DOM
function vaciarCarrito(){
	while(listaCursos.firstChild){
		listaCursos.removeChild(listaCursos.firstChild);
	}
	vaciarLocalStorage();
	return false;
}

//Almacena cursos en el carrito a Local Storage
function guardarCursoLocalStorage(curso){
	let cursos;
	cursos = obtenerCursosLocalStorage();
	cursos.push(curso);
	localStorage.setItem('cursos',JSON.stringify(cursos));
}

//Comprueba elementos en localStorage
function obtenerCursosLocalStorage(){
	let cursosLS;
	if(localStorage.getItem('cursos') === null){
		cursosLS = [];
	}else{
		cursosLS = JSON.parse(localStorage.getItem('cursos'));
	}
	return cursosLS;
}


//Carga los cursos desde LocalStorage
function leerStorageListo(){
	let cursosLS;
	cursosLS = obtenerCursosLocalStorage();
	
	cursosLS.forEach(function(curso){
		const row = document.createElement('tr');
		row.innerHTML = `
			<td><img src="${curso.imagen}" width=100></td>
			<td>${curso.titulo}</td>
			<td>${curso.precio}</td>
			<td><a href="#" class="borrar-curso" data-id="${curso.id}">X</a></td>
		`;
		listaCursos.appendChild(row);
	});	
}

//Eliminar el curso por el ID en LocalStorage
function eliminarCursoLocalStorage(cursoID){
	let cursosLS;
	cursosLS = obtenerCursosLocalStorage();
	cursosLS.forEach(function(curso,index){
		if(curso.id === cursoID){
			cursosLS.splice(index,1);	
		}
	});
	localStorage.setItem('cursos',JSON.stringify(cursosLS));
}

//Elimina todos los cursos de localStorage
function vaciarLocalStorage(){
	localStorage.clear();
}